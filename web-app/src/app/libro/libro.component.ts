import { Component, OnInit } from '@angular/core';

import { RestService, Libro } from '../rest.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.scss']
})
export class LibroComponent implements OnInit {

  libros: Libro[] = [];

  constructor(
    public rest: RestService,
    private router: Router) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.rest.getLibros().subscribe((resp: any) => {
      this.libros = resp;
      console.log(this.libros);
    });
  }

  add(): void {
    this.router.navigate(['/libro-add']);
  }

  search(): void {
    this.router.navigate(['/libro-search']);
  }

  delete(id: string): void {
    this.rest.deleteLibro(id)
      .subscribe(() => {
          this.getProducts();
        }, (err) => {
          console.log(err);
        }
      );
  }

}
