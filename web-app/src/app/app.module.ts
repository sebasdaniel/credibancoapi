import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { LibroComponent } from './libro/libro.component';
import { LibroAddComponent } from './libro-add/libro-add.component';
import { LibroDetailComponent } from './libro-detail/libro-detail.component';
import { LibroEditComponent } from './libro-edit/libro-edit.component';
import { LibroSearchComponent } from './libro-search/libro-search.component';

@NgModule({
  declarations: [
    AppComponent,
    LibroComponent,
    LibroAddComponent,
    LibroDetailComponent,
    LibroEditComponent,
    LibroSearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
