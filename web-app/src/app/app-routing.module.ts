import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LibroComponent } from './libro/libro.component';
import { LibroDetailComponent } from './libro-detail/libro-detail.component';
import { LibroAddComponent } from './libro-add/libro-add.component';
import { LibroEditComponent } from './libro-edit/libro-edit.component';
import { LibroSearchComponent } from './libro-search/libro-search.component';
import { title } from 'process';

const routes: Routes = [
  {
    path: 'libros',
    component: LibroComponent,
    data: { title: 'Listado de Libros' }
  },
  {
    path: 'libros-details/:id',
    component: LibroDetailComponent,
    data: { title: 'Detalle del Libro' }
  },
  {
    path: 'libro-add',
    component: LibroAddComponent,
    data: { title: 'Agregar Libro' }
  },
  {
    path: 'libro-edit/:id',
    component: LibroEditComponent,
    data: { title: 'Editar Libro' }
  },
  {
    path: 'libro-search',
    component: LibroSearchComponent,
    data: { title: 'Buscar Libros'}
  },
  { path: '',
    redirectTo: '/libros',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
