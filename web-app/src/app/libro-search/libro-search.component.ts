import { Component, OnInit, Input } from '@angular/core';
import { RestService, Autor, Libro } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-libro-search',
  templateUrl: './libro-search.component.html',
  styleUrls: ['./libro-search.component.scss']
})
export class LibroSearchComponent implements OnInit {

  @Input() searchData = { titulo: null, ano: null, idAutor: null };
  autores: Autor[] = [];
  libros: Libro[] = [];

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getAutores();
  }

  getAutores(): void {
    this.rest.getAutores().subscribe((resp: any) => {
      this.autores = resp;
      console.log(this.autores);
    });
  }

  searchLibro(): void {
    if (this.searchData.ano == null && this.searchData.titulo == null && this.searchData.idAutor == null) {
      alert('Debe ingresar al menos un parametro de busqueda');
    }
    console.log('searchData:');
    console.log(this.searchData);
    this.rest.searchLibro(this.searchData).subscribe((resp: any) => {
      this.libros = resp;
      console.log(this.libros);
    }, (err) => {
      console.log(err);
    });
  }

}
