import { Component, OnInit, Input } from '@angular/core';
import { RestService, Autor, Editorial } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-libro-add',
  templateUrl: './libro-add.component.html',
  styleUrls: ['./libro-add.component.scss']
})
export class LibroAddComponent implements OnInit {

  @Input() autorData = { id: 0 };
  @Input() editorialData = { id: 0};
  @Input() libroData = { titulo: '', ano: null, genero: '', numeroPaginas: 0, autor: null, editorial: null };

  editoriales: Editorial[] = [];
  autores: Autor[] = [];

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getAutores();
    this.getEditoriales();
  }

  getAutores(): void {
    this.rest.getAutores().subscribe((resp: any) => {
      this.autores = resp;
      console.log(this.autores);
    });
  }

  getEditoriales(): void {
    this.rest.getEditoriales().subscribe((resp: any) => {
      this.editoriales = resp;
      console.log(this.editoriales);
    });
  }

  addLibro(): void {
    if (this.autorData.id != 0) {
      this.libroData.autor = this.autorData;
    }
    if (this.editorialData.id != 0) {
      this.libroData.editorial = this.editorialData;
    }
    console.log('libroData:');
    console.log(this.libroData);
    this.rest.addLibro(this.libroData).subscribe((result) => {
      //this.router.navigate(['/libro-details/' + result._id]);
      this.router.navigate(['/libros']);
    }, (err) => {
      console.log(err);
    });
  }

}
