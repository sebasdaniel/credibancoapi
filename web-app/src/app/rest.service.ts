import { Injectable } from '@angular/core';

import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Autor {
  _id: number;
  nombreCompleto: string;
  fechaNacimiento: Date;
}

export interface Editorial {
  _id: number;
  nombre: string;
}

export interface Libro {
  _id: number;
  titulo: string;
  ano: number;
  genero: string;
  numero_paginas: number;
  editorial: Editorial;
  autor: Autor;
}

const endpoint = 'http://127.0.0.1:8080/';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response): any {
    const body = res;
    return body || { };
  }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  // #### API related functions ####
  // Libros
  getLibros(): Observable<any> {
    return this.http.get<Libro>(endpoint + 'libro').pipe(
      catchError(this.handleError)
    );
  }

  getLibro(id: string): Observable<any> {
    return this.http.get<Libro>(endpoint + 'libro/' + id).pipe(
      catchError(this.handleError)
    );
  }

  addLibro(libro: any): Observable<any> {
    return this.http.post(endpoint + 'libro', libro).pipe(
      catchError(this.handleError)
    );
  }

  updateLibro(id: string, libro: Libro): Observable<any> {
    return this.http.put<Libro>(endpoint + 'libro/' + id, libro).pipe(
      catchError(this.handleError)
    );
  }

  deleteLibro(id: string): Observable<any> {
    return this.http.delete<Libro>(endpoint + 'libro/' + id).pipe(
      catchError(this.handleError)
    );
  }

  searchLibro(searchDto: any): Observable<any> {
    return this.http.post<Libro>(endpoint + 'libro/search', searchDto).pipe(
      catchError(this.handleError)
    );
  }

  // Autores
  getAutores(): Observable<any> {
    return this.http.get<Autor>(endpoint + 'autor').pipe(
      catchError(this.handleError)
    );
  }

  addAutor(autor: any): Observable<any> {
    return this.http.post(endpoint + 'autor', autor).pipe(
      catchError(this.handleError)
    );
  }

  // Editoriales
  getEditoriales(): Observable<any> {
    return this.http.get<Editorial>(endpoint + 'editorial').pipe(
      catchError(this.handleError)
    );
  }

  addEditorial(editorial: any): Observable<any> {
    return this.http.post(endpoint + 'editorial', editorial).pipe(
      catchError(this.handleError)
    );
  }

}
