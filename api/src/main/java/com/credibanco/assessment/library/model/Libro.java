package com.credibanco.assessment.library.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Entity
@Table
public class Libro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message = "El titulo es obligatorio")
	@NotEmpty(message = "El titulo no debe estar vacio")
	private String titulo;
	
	@Positive(message = "El ano debe ser un numero positivo")
	private Integer ano;
	
	@NotNull(message = "El genero es obligatorio")
	@NotEmpty(message = "EL genero no debe estar vacio")
	private String genero;
	
	@NotNull(message = "El numero de paginas es obligatorio")
	@Positive(message = "El numero de paginas debe ser un numero positivo")
	private Integer numeroPaginas;
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "editorial_id")
	private Editorial editorial;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "autor_id")
	private Autor autor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public Editorial getEditorial() {
		return editorial;
	}

	public void setEditorial(Editorial editorial) {
		this.editorial = editorial;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}
	
}
