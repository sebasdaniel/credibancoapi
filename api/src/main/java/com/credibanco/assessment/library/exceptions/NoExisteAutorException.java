package com.credibanco.assessment.library.exceptions;

public class NoExisteAutorException extends Exception {
    
    public NoExisteAutorException(String message) {
        super(message);
    }
}
