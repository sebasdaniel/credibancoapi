package com.credibanco.assessment.library.exceptions;

public class MaximoLibrosRegistradoException extends Exception {
    
    public MaximoLibrosRegistradoException(String message) {
        super(message);
    }
}
