package com.credibanco.assessment.library.exceptions;

public class NoExisteEditorialException extends Exception {
    
    public NoExisteEditorialException(String message) {
        super(message);
    }
}
