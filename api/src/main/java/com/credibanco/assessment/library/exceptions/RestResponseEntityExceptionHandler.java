package com.credibanco.assessment.library.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {
        MaximoLibrosRegistradoException.class,
        NoExisteAutorException.class,
        NoExisteEditorialException.class
    })
    protected ResponseEntity<Object> handleConflict(Exception ex, WebRequest request) {

        String bodyOfResponse = ex.getMessage();

        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if (ex instanceof MaximoLibrosRegistradoException) {
            httpStatus = HttpStatus.PRECONDITION_FAILED;
        } else if (ex instanceof NoExisteAutorException || ex instanceof NoExisteEditorialException) {
            httpStatus = HttpStatus.NOT_FOUND;
        }

        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), httpStatus, request);
    }
    
}
