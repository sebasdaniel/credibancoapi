package com.credibanco.assessment.library.service;

import java.util.List;

import com.credibanco.assessment.library.dto.BusquedaLibroDTO;
import com.credibanco.assessment.library.exceptions.MaximoLibrosRegistradoException;
import com.credibanco.assessment.library.exceptions.NoExisteAutorException;
import com.credibanco.assessment.library.exceptions.NoExisteEditorialException;
import com.credibanco.assessment.library.model.Libro;

public interface ILibroService {

	List<Libro> buscarTodos();

	List<Libro> buscarPorParametros(BusquedaLibroDTO busqueda);
	
	Libro guardar(Libro libro) throws MaximoLibrosRegistradoException, NoExisteAutorException, NoExisteEditorialException;
}
