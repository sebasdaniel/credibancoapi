package com.credibanco.assessment.library.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.credibanco.assessment.library.model.Editorial;
import com.credibanco.assessment.library.repository.EditorialRepository;
import com.credibanco.assessment.library.service.IEditorialService;

@Service
public class EditorialServiceImpl implements IEditorialService {

	@Autowired
	private EditorialRepository repoEditorial;


	@Override
	public List<Editorial> buscarTodos() {
		return repoEditorial.findAll();
	}
	

	@Override
	public Editorial guardar(Editorial editorial) {
		
		editorial.setId(null);
		return repoEditorial.saveAndFlush(editorial);
	}

}
