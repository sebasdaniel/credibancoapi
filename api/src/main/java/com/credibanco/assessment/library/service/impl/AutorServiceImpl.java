package com.credibanco.assessment.library.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.credibanco.assessment.library.model.Autor;
import com.credibanco.assessment.library.repository.AutorRepository;
import com.credibanco.assessment.library.service.IAutorService;

@Service
public class AutorServiceImpl implements IAutorService {
	
	@Autowired
	private AutorRepository repoAutor;


	@Override
	public List<Autor> buscarTodos() {
		return repoAutor.findAll();
	}

	
	@Override
	public Autor guardar(Autor autor) {
		
		autor.setId(null);
		return repoAutor.saveAndFlush(autor);
	}

}
