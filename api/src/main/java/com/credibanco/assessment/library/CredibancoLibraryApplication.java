package com.credibanco.assessment.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CredibancoLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(CredibancoLibraryApplication.class, args);
	}

}
