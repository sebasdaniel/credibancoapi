package com.credibanco.assessment.library.dto;

public class BusquedaLibroDTO {
    
    private Integer idAutor;
    private String titulo;
    private Integer ano;

    public BusquedaLibroDTO() {
    }

    public BusquedaLibroDTO(Integer idAutor, String titulo, Integer ano) {
        this.idAutor = idAutor;
        this.titulo = titulo;
        this.ano = ano;
    }

    public Integer getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(Integer idAutor) {
        this.idAutor = idAutor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }
    
}
