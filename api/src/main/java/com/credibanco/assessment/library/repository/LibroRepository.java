package com.credibanco.assessment.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.credibanco.assessment.library.model.Libro;

public interface LibroRepository extends JpaRepository<Libro, Integer> {

    Integer countByEditorialId(Integer idEditorial);
}
