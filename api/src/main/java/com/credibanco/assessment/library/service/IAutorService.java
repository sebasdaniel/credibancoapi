package com.credibanco.assessment.library.service;

import java.util.List;

import com.credibanco.assessment.library.model.Autor;

public interface IAutorService {

	List<Autor> buscarTodos();
	
	Autor guardar(Autor autor);
}
