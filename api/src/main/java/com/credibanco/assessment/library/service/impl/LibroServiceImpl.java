package com.credibanco.assessment.library.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers;
import org.springframework.stereotype.Service;

import java.util.List;

import com.credibanco.assessment.library.dto.BusquedaLibroDTO;
import com.credibanco.assessment.library.exceptions.MaximoLibrosRegistradoException;
import com.credibanco.assessment.library.exceptions.NoExisteAutorException;
import com.credibanco.assessment.library.exceptions.NoExisteEditorialException;
import com.credibanco.assessment.library.model.Autor;
import com.credibanco.assessment.library.model.Editorial;
import com.credibanco.assessment.library.model.Libro;
import com.credibanco.assessment.library.repository.AutorRepository;
import com.credibanco.assessment.library.repository.EditorialRepository;
import com.credibanco.assessment.library.repository.LibroRepository;
import com.credibanco.assessment.library.service.ILibroService;

@Service
public class LibroServiceImpl implements ILibroService {

	@Autowired
	private LibroRepository repoLibro;

	@Autowired
	private AutorRepository repoAutor;

	@Autowired
	private EditorialRepository repoEditorial;


	@Override
	public List<Libro> buscarTodos() {
		return repoLibro.findAll();
	}


	@Override
	public List<Libro> buscarPorParametros(BusquedaLibroDTO busqueda) {
		
		Libro example = new Libro();

		example.setTitulo(busqueda.getTitulo());
		example.setAno(busqueda.getAno());

		if (busqueda.getIdAutor() != null) {
			Autor autor = new Autor();
			autor.setId(busqueda.getIdAutor());
			example.setAutor(autor);
		}

		// match ignoring case and containing (sql like)
		ExampleMatcher matcher = ExampleMatcher
			.matchingAll()
			.withMatcher("titulo", GenericPropertyMatchers.contains().ignoreCase());

		return repoLibro.findAll(Example.of(example, matcher));
	}


	@Override
	public Libro guardar(Libro libro)
			throws MaximoLibrosRegistradoException, NoExisteAutorException, NoExisteEditorialException {

		// validacion autor
		if (libro.getAutor() == null) {
			throw new NoExisteAutorException("El autor no esta registrado");
		}

		repoAutor.findById(libro.getAutor().getId())
				.orElseThrow(() -> new NoExisteAutorException("El autor no esta registrado"));

		// si se establece la editorial
		Editorial editorial = libro.getEditorial();

		if (editorial != null) {

			// validacion editorial y optencion de todos sus datos
			editorial = repoEditorial.findById(libro.getEditorial().getId())
					.orElseThrow(() -> new NoExisteEditorialException("La editorial no esta registrada"));

			// validacion maximo permitido
			int editorialTotalLibrosActuales = repoLibro.countByEditorialId(editorial.getId());

			if (editorial.getMaximoLibrosRegistrados() <= editorialTotalLibrosActuales) {
				throw new MaximoLibrosRegistradoException(
						"No es posible registrar el libro, se alcanzo el maximo permitido");
			}
		}

		// save libro
		libro.setId(null);

		return repoLibro.saveAndFlush(libro);
	}

}
