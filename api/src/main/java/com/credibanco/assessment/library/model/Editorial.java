package com.credibanco.assessment.library.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Editorial {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message = "EL nombre es obligatorio")
	@NotEmpty(message = "El nombre no puede estar vacio")
	private String nombre;
	
	@NotNull(message = "La direccion de correspondencia es obligatoria")
	@NotEmpty(message = "La direccion de correspondencia no debe estar vacia")
	private String direccionCorrespondencia;
	
	@NotNull(message = "El telefono es obligatorio")
	@NotEmpty(message = "El telefono no debe estar vacio")
	private String telefono;
	
	@Email(message = "Debe ingresar una direccion de correo electronico")
	private String correo;
	
	@NotNull(message = "El numero maximo de libros registrados es obligatorio")
	@Min(-1)
	private Integer maximoLibrosRegistrados;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccionCorrespondencia() {
		return direccionCorrespondencia;
	}

	public void setDireccionCorrespondencia(String direccionCorrespondencia) {
		this.direccionCorrespondencia = direccionCorrespondencia;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getMaximoLibrosRegistrados() {
		return maximoLibrosRegistrados;
	}

	public void setMaximoLibrosRegistrados(Integer maximoLibrosRegistrados) {
		this.maximoLibrosRegistrados = maximoLibrosRegistrados;
	}
	
}
