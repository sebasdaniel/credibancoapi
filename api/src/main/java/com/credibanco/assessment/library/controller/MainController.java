package com.credibanco.assessment.library.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.credibanco.assessment.library.dto.BusquedaLibroDTO;
import com.credibanco.assessment.library.exceptions.MaximoLibrosRegistradoException;
import com.credibanco.assessment.library.exceptions.NoExisteAutorException;
import com.credibanco.assessment.library.exceptions.NoExisteEditorialException;
import com.credibanco.assessment.library.model.Autor;
import com.credibanco.assessment.library.model.Editorial;
import com.credibanco.assessment.library.model.Libro;
import com.credibanco.assessment.library.service.IAutorService;
import com.credibanco.assessment.library.service.IEditorialService;
import com.credibanco.assessment.library.service.ILibroService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://127.0.0.1:4200")
public class MainController {

    @Autowired
    private ILibroService serviceLibro;

    @Autowired
    private IAutorService serviceAutor;

    @Autowired
    private IEditorialService serviceEditorial;


    /**
     * Muestra el listado de todos los autores.
     * 
     * @return
     */
    @GetMapping("/autor")
    public List<Autor> listAutor() {
        return serviceAutor.buscarTodos();
    }


    /**
     * Valida los datos requeridos del autor y lo guarda.
     * 
     * @param autor el Autor a guardar
     * @return el Autor con su repectivo id
     */
    @PostMapping("/autor")
    public Autor saveAutor(@Valid @RequestBody Autor autor) {
        return serviceAutor.guardar(autor);
    }


    /**
     * Listado de editoriales.
     * 
     * @return
     */
    @GetMapping("/editorial")
    public List<Editorial> listEditorial() {
        return serviceEditorial.buscarTodos();
    }


    /**
     * Valida los datos requeridos de la editorial y la guarda.
     * 
     * @param editorial la Editorial a guardar
     * @return la Editorial incluyendo su id
     */
    @PostMapping("/editorial")
    public Editorial saveEditorial(@Valid @RequestBody Editorial editorial) {
        return serviceEditorial.guardar(editorial);
    }


    /**
     * Retorna el listado de todos los libros.
     * 
     * @return
     */
    @GetMapping("/libro")
    public List<Libro> listLibro() {
        return serviceLibro.buscarTodos();
    }


    /**
     * Valida que el libro cumpla con los parametros requeridos y lo guarda.
     * 
     * @param libro el libro a guardar
     * @return el libro guardado con el id de sesion
     * @throws NoExisteEditorialException
     * @throws NoExisteAutorException
     * @throws MaximoLibrosRegistradoException
     */
    @PostMapping("/libro")
    public Libro saveLibro(@Valid @RequestBody Libro libro)
            throws MaximoLibrosRegistradoException, NoExisteAutorException, NoExisteEditorialException {
        return serviceLibro.guardar(libro);
    }


    /**
     * Busca libros por parametros como autor, ano y titulo.
     * 
     * @param busqueda
     * @return
     */
    @PostMapping("/libro/search")
    public List<Libro> searchLibro(@RequestBody BusquedaLibroDTO busqueda) {
        return serviceLibro.buscarPorParametros(busqueda);
    }


    /**
     * Custom Exception handler para cuando hay un bad request.
     * 
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {

        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }

}
