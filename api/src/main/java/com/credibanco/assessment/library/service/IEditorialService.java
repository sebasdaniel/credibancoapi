package com.credibanco.assessment.library.service;

import java.util.List;

import com.credibanco.assessment.library.model.Editorial;

public interface IEditorialService {

	List<Editorial> buscarTodos();
	
	Editorial guardar(Editorial editorial);
}
